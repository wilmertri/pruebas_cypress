describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        //Lineas nuevas  
        cy.contains('Ingresar').click()
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Fabian")
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Perez")
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("fabiantriana1072@gmail.com")
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Ingeniería Civil')
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("123456789")
        cy.get('.cajaSignUp').find('input[name="acepta"]').click()
        cy.get('.cajaSignUp').contains('Registrarse').click()
        cy.contains("Error: Ya existe un usuario registrado con el correo 'fabiantriana1072@gmail.com'")
    })
})